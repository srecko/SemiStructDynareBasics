# Semi-Structural Modeling in Dynare

This repository contains examples and scripts for semi-structural economic modeling using Dynare. The examples demonstrate key steps in setting up, estimating, and simulating a basic economic model.

## Repository Structure

- `/data`:
  - `load_data.m`: This script loads data from an Excel file into a `dseries` database. Ensure your Excel data is formatted with variable names in the first row and dates in the first column.

- `/main`:
  - `consumption.m`: This MATLAB script sets up the consumption model component of the Dynare model.
  - `consumption.mod`: This is the Dynare model file for the consumption component of the economic model.

- `/simulation/identities`:
  - Contains files that define the identities of the model. These are crucial for ensuring that the model's structure and relationships are correctly specified.


- `driver_forecast.m`: Located in the main folder, this script performs several key functions:
  - Generates additional data required for the model.
  - Sets up the model parameters and structure.
  - Inverts the residuals.
  - Creates a simple example forecast to illustrate the model's predictive capabilities.

## Usage

1. Run `load_data.m` in the `/data` folder to import and prepare your time series data.
2. Execute `consumption.m` in the `/main` folder to set up and estimate the consumption component of the model and create .inc files.
3. Use `driver_forecast.m` to simulate forecasts and analyze the model's output.

Ensure you have Dynare and MATLAB/Octave installed and properly configured to run these scripts. The programs run with Dynare 5.5 

## Contributions and Contact

This repository is managed by Srecko Zimic, European central Bank. For any questions or contributions, please reach out to srecko.zimic@gmail.com.

