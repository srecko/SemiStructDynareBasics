% The example driver to produce the two illustrative forecasts and the IRFs 

% load the database 
dbase = dseries('model_data_updated.mat');

dbase = dbase(dates('2008Q1'):dates('2018Q4')); 
dbase{'res_U2_HH_COR'} = [];

% Aggregate the results to obtain final .mod file. The first argument defines the name and location of the aggregate
% mod file.
inputs_for_aggregate = {[cd,'\simulation\estimation'];
                        [cd,'\simulation\identities']};

ROOT_FOLDER = cd; 

aggregate(sprintf('%s/examples/consumption_joint.mod',ROOT_FOLDER), {'stochastic,json=compute'}, cd, inputs_for_aggregate{:});

% work in the folder where the produced aggregate mod file is located.
cd(sprintf('%s/examples/', ROOT_FOLDER))

% Run dynare (required to get the list of variables in the model).
dynare('consumption_joint', 'stochastic', 'nopreprocessoroutput', 'notime')

% Set options for the nonlinear solver (here we use solve_algo=14, tailored for semi structural models, which is faster
% than Dynare's default algorithm).
options.solve_algo = 14;
options.maxiter = 500;
options.tolx = 1e-6;
options.tolf = 1e-6;

% Compute inverted residuals
dbase = equation.evaluate(dbase, {'U2_CONSUMPTION_PAC_PE'}); 
[dbase, info] = checkdatabaseforinversion(dbase(dates('2009Q1'):dates('2018Q4')), M_);
[residuals, info] = calibrateresiduals(dbase(dates('2009Q3'):dates('2018Q4')),info, M_); 

%% Example 1: In-Sample Forecast 

start_date = dates('2013Q1'); 
end_date   = dates('2015Q4'); 
dbase_full = merge(dbase,residuals);
innovations = dbase_full{M_.exo_names{:}}; 
all_vars = [setdiff(M_.exo_names,residuals.name);M_.endo_names]; 
dbase_mod  = dbase{all_vars{:}}; 

% setting residuals to zero
innovations.res_U2_HH_COR = innovations.res_U2_HH_COR*0;
simulation = simul_backward_model(dbase_mod(firstobservedperiod(dbase_mod):start_date-1), length(start_date:end_date), innovations(start_date:end_date));

% Ploting 
figure; 
subplot(1,2,1)
plot([ygrowth(dbase.U2_HH_COR(start_date-4:end_date)).data*100 ygrowth(simulation.U2_HH_COR(start_date-4:end_date)).data*100])
title('Consumption growth')
legend('Baseline','Forecast','Location','Southeast')
subplot(1,2,2)
plot([ygrowth(dbase.U2_YER(start_date-4:end_date)).data*100 ygrowth(simulation.U2_YER(start_date-4:end_date)).data*100])
title('GDP growth')
legend('Baseline','Forecast','Location','Southeast')

%% Example 2: Out-of-Sample Forecast 

start_date_f = lastobservedperiod(dbase_full)+1; 
end_date_f   = start_date_f + 11; 

% extend the exogenous variables (but not residuals) with constant growth rate 
exxo_vars = dbase_full{M_.exo_names{:}}; 
exxo_vars{'[res_.*]'} = []; exxo_varsTemp = [];
for v = 1:size(exxo_vars.name,1)
    var_name    = exxo_vars.name{v,1}; 
    qg_var      = qgrowth(exxo_vars{var_name}); 
    qg_var_mean = mean(qg_var(start_date_f-13:start_date_f-1).data); 

    Ts               = end_date_f - start_date_f+2;
    spliced_var      = zeros(Ts,1); 
    spliced_var(1,1) = exxo_vars{var_name}(start_date_f-1).data; 
    for d = 2:Ts
        spliced_var(d,1) = spliced_var(d-1,1)*(1+qg_var_mean);
    end

    exxo_varsTemp = dseries(spliced_var(2:end,1),start_date_f:end_date_f,var_name); 
    exxo_vars = merge(exxo_varsTemp,exxo_vars); 
end

% set residulals to zero 
res_names = setdiff(M_.exo_names,exxo_vars.name); 
residuals = dbase_full{res_names{:}}; 
for v = 1:size(res_names,1)
    Ts               = end_date_f - start_date_f+2;   
    resTemp   = dseries(zeros(Ts-1,1),start_date_f:end_date_f,res_names(v,1)); 
    residuals = merge(resTemp,residuals); 
end

% join exogenous and residuals 
innovationsF  = merge(exxo_vars,residuals); 

% set to variables to constant 
var_const_names = {'res_U2_YER';'U2_H_Q_YER400';'res_U2_HH_DIR'}; 
for v = 1:size(var_const_names,1)
innovationsF{var_const_names{v,1}}   = dseries(innovationsF{var_const_names{v,1}}(start_date_f-1).data*ones(Ts-1,1),start_date_f:end_date_f,var_const_names(v,1)); 
end

% simulate the model 
simulationF = simul_backward_model(dbase_full(firstobservedperiod(dbase_full):start_date_f-1), length(start_date_f:end_date_f), innovationsF(start_date_f:end_date_f));

% define the variables to be ploted in y-o-y growth rates
Y1 = ygrowth(simulationF.U2_HH_COR)*100; 
Y2 = ygrowth(simulationF.U2_YER)*100; 

% Ploting 
figure; 
subplot(1,2,1)
plot(start_date_f-12:start_date_f-1,Y1(start_date_f-12:start_date_f-1).data), axis tight; 
hold on 
plot(start_date_f-1:end_date_f,Y1(start_date_f-1:end_date_f).data,'--'), axis tight; 
title('Consumption growth')
legend('Baseline','Forecast','Location','Southeast')
subplot(1,2,2)
plot(start_date_f-12:start_date_f-1,Y2(start_date_f-12:start_date_f-1).data), axis tight; 
hold on 
plot(start_date_f-1:end_date_f,Y2(start_date_f-1:end_date_f).data,'--'), axis tight; 
title('GDP growth')
legend('Baseline','Forecast','Location','Southeast')

%% Example 3: IRFs 

% Set the number of periods for the IRFs
periods         = 20; 
start_date_IRFs = dates('2013Q1'); 

% using previously computed inverted database 
innovationsIRFs = dbase_full{M_.exo_names{:}};

% 10% permanent shock to the government consumption 
innovationsIRFs.U2_GO_COR = innovationsIRFs.U2_GO_COR*1.1;

simulationIRFs = simul_backward_model(dbase_full(firstobservedperiod(dbase_full):start_date_IRFs-1), length(start_date_IRFs:start_date_IRFs+periods), innovationsIRFs(start_date_IRFs:start_date_IRFs+periods));

list_var_IRFs = {'U2_GO_COR','U2_YER','U2_HH_COR'};
IRFs          = (log(simulationIRFs{list_var_IRFs{:}}(start_date_IRFs-4:start_date_IRFs+periods)) - log(dbase_full{list_var_IRFs{:}}(start_date_IRFs-4:start_date_IRFs+periods)))*100; 

% Ploting 
figure; 
subplot(1,3,1)
plot(IRFs.U2_GO_COR), axis tight; 
title('Government Consumption (% dev.)')
legend('IRFs','Location','Southeast')
subplot(1,3,2)
plot(IRFs.U2_HH_COR), axis tight; 
title('Consumption (% dev.)')
legend('IRFs','Location','Southeast')
subplot(1,3,3)
plot(IRFs.U2_YER), axis tight; 
title('GDP (% dev.)')
legend('IRFs','Location','Southeast')