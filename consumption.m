% Consumption estimation file 

nls= true;             % Estimate PAC equation by NLS.
targetOG = true;      % Estimate target using BOLS

% Load database.
dbase = dseries('data\model_data.mat');

% generate tmp consumption data for estimation of the target consumption
dbase.U2_HH_COR_tmp = dbase.U2_HH_COR; 

% Set estimation data ranges.
rangeExtVar = dates('2000Q1'):dates('2018Q4');
rangeOLS    = dates('2000Q1'):dates('2018Q4');
rangePAC    = dates('2001Q2'):dates('2017Q4');

%
% Call Dynare.
%

% Clean previously generated folders, if any.
if isfolder(sprintf('%s/+consumption', pwd()))
    rmdir('+consumption', 's');
end

if isfolder(sprintf('%s/consumption', pwd()))
    rmdir('consumption', 's');
end

% Call dynare
dynare('consumption', 'nopreprocessoroutput', 'notime') %, sprintf('-I%s/blocks', ROOT_FOLDER)

% Estimate target consumption 
dbase = dyn_ols(dbase, {'U2_HH_OCOR_TARGET', 'U2_HH_OCOR', 'exp'}, {'U2_HH_OCOR_TARGET'}, {}, {{'u2_consumption_tar_const'}}, rangeOLS); % Run dyn_ols to get the parameter ordering and potential initialization


% SUR estimation of the extended VAR %strcat('U2_', {'HH_OCOR', 'H_Q_YER400'})
sur(dbase, M_.param_names(1:2), strcat('U2_', {'HH_OCOR', 'H_Q_YER400'}), 'U2_HH_OCOR', true, rangeExtVar);

%
% PAC ESTIMATION - CONSUMPTION
%

% Append the database with missing residuals for PAC estimation
dbase.res_U2_HH_COR = dseries(nan(dbase.nobs, 1), dbase.dates(1), 'res_U2_HH_COR');

% Initialize the PAC model (build the Companion VAR representation for the auxiliary model).
pac.initialize('pac_consumption');

% Update the parameters of the PAC expectation model (h0 and h1 vectors).
pac.update.expectation('pac_consumption');

% Run either NLS or ITERATIVE OLS estimation
if nls
	pac.estimate.nls('U2_CONSUMPTION_PAC', eparams, dbase, rangePAC, 'annealing');
else
	pac.estimate.iterative_ols('U2_CONSUMPTION_PAC', eparams, dbase, rangePAC);
end

pac.print('pac_consumption', 'U2_CONSUMPTION_PAC');

% Cherry-pick variables that are estimated
consumption_vars_simul = strcat('U2_',{'CONSUMPTION_PAC', 'HH_OCOR_TARGET'});
cherrypick('consumption', [cd,'/simulation/estimation/'], consumption_vars_simul, false);

% Save appended database and clean the folder
dbase.save('model_data_updated');