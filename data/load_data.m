% Example script to import data from Excel to Dseries

% Define the path to the Excel file
path = 'model_data.xlsx';

% Read the data from Excel
[~, ~, raw] = xlsread(path);

% Extract variable names, dates, and data
Var_names = raw(1, 2:end); 
Dates     = raw(2:end, 1);
Data      = raw(2:end, 2:end); 

% Create a dseries object with the data
model_data = dseries(cell2mat(Data), dates(Dates{1}):dates(Dates{end}), Var_names); 

model_data.save('model_data');