// --+ options: stochastic,transform_unary_ops,json=compute +--

// Define list of parameters entering the PAC equation
@#define init_param_list = ["u2_beta_discount_consumption", "u2_ecm_pac_consumption", "u2_hh_cor_pac_u2_cor_L1"]

// Initialise the parameters entering the PAC equation
@#define init_param_val = [0.98, 0.2, 0.8]

// Construct the list of parameters to be estimated in PAC equation
eparams = struct();
@#for i in 2:length(init_param_list)
eparams.@{init_param_list[i]}=@{init_param_val[i]};
@#endfor

var
U2_HH_OCOR
U2_HH_COR
U2_HH_COR_tmp
U2_H_Q_YER400
;

varexo
U2_HH_DIR
res_U2_HH_OCOR
res_U2_HH_COR
res_U2_H_Q_YER
;

parameters
u2_hh_ocor_ecm_u2_hh_ocor_L1
u2_hh_ocor_u2_hh_ocor_L1
u2_consumption_tar_const
@#for el in init_param_list
@{el}
@#endfor

u2_hh_dir
;

// values of parameters 
@#for el in 1:length(init_param_list)
@{init_param_list[el]} = @{init_param_val[el]};
@#endfor

u2_hh_dir = 1;

// Auxiliary model for PAC expectation.
trend_component_model(model_name=tc_consumption, eqtags=['U2_HH_OCOR','U2_H_Q_YER400'], targets=['U2_H_Q_YER400']);

// Define PAC model
pac_model(auxiliary_model_name=tc_consumption, discount=u2_beta_discount_consumption, model_name=pac_consumption, growth = U2_H_Q_YER400(-1));

model;

  [name='U2_HH_OCOR']
  diff(diff(log(U2_HH_OCOR))) =  u2_hh_ocor_ecm_u2_hh_ocor_L1 * (diff(log(U2_HH_OCOR(-1))) - U2_H_Q_YER400(-1))
  + u2_hh_ocor_u2_hh_ocor_L1 * diff(diff(log(U2_HH_OCOR(-1))))
  + res_U2_HH_OCOR;

  [name='U2_H_Q_YER400']
  U2_H_Q_YER400 = U2_H_Q_YER400(-1) + res_U2_H_Q_YER;

  [name='U2_CONSUMPTION_PAC']
  diff(log(U2_HH_COR)) =   u2_ecm_pac_consumption*(log(U2_HH_OCOR(-1)) - log(U2_HH_COR(-1)))
  + u2_hh_cor_pac_u2_cor_L1*diff(log(U2_HH_COR(-1)))
  + pac_expectation(pac_consumption)
  + res_U2_HH_COR;

  [name='U2_HH_OCOR_TARGET', rename='U2_HH_COR_tmp->U2_HH_OCOR']
  log(U2_HH_COR_tmp) =  u2_consumption_tar_const
  + u2_hh_dir * log(U2_HH_DIR)
  + res_U2_HH_OCOR;

end;